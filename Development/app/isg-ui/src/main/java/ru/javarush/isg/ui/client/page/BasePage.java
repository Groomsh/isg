package ru.javarush.isg.ui.client.page;

import ru.javarush.isg.ui.client.presenter.IPresenter;

/**
 * Use this class instead ABasePage for some logic;
 */
public class BasePage extends ABasePage {

    @SuppressWarnings("all")
    private IPresenter presenter;

    public BasePage(IPresenter presenter) {
        super();
        this.presenter = presenter;
    }

}
