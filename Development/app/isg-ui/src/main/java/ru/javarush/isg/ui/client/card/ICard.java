package ru.javarush.isg.ui.client.card;

import com.google.gwt.user.client.ui.IsWidget;

public interface ICard extends IsWidget {

    /**
     * Reset the inner widgets state to default
     */
    public void clearState();

}
