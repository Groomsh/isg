package ru.javarush.isg.ui.client.presenter;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.ui.Widget;
import ru.javarush.isg.ui.client.panel.MenuPanel;

@SuppressWarnings("all")
public class MenuPresenter extends APresenter {

    private String role;
    private MenuPanel menuPanel;

    public MenuPresenter(String role) {
        this.role = role;
        buildMenu(role);
    }

    public void buildMenu(String role) {
        //TODO build menu by Role (unimplemented)
        menuPanel = new MenuPanel();
    }


    @Override
    public Widget getView() {
        return menuPanel.asWidget();
    }

    @Override
    protected GwtEvent.Type[] getTypes() {
        //TODO unimplemented
        return new GwtEvent.Type[0];
    }
}
