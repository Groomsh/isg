package ru.javarush.isg.ui.client.presenter;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.ui.*;
import ru.javarush.isg.ui.client.page.BasePage;
import ru.javarush.isg.ui.client.page.IBasePage;
import ru.javarush.isg.ui.shared.event.CredentialsValidatedEvent;

@SuppressWarnings("unused")
public class BasePresenter extends APresenter implements CredentialsValidatedEvent.CredentialsValidatedEventHandler {

    private final PageBuilder builder = new PageBuilder();

    public BasePresenter() {}

    @Override
    protected GwtEvent.Type[] getTypes() {
        return new GwtEvent.Type[]{CredentialsValidatedEvent.TYPE};
    }

    @Override
    public void onCredentialsValidated(CredentialsValidatedEvent event) {
        builder.createHeader(new MenuPresenter(event.getCredentials().getRole()).getView());
        builder.createCreateTaskPage(new Button("West Stub"), new CreateTaskPresenter().getView()); //TODO реализовать меню слева
    }

    @Override
    public void update() {
        builder.createLoginPage(new LoginPresenter().getView());
    }

    /**
     * Delegate all add/remove logic to this Class;
     */
    private class PageBuilder implements IBasePage {

        private IBasePage layoutPage;

        public void createHeader(Widget header) {
            addNorth(header, HasHorizontalAlignment.ALIGN_LEFT, HasVerticalAlignment.ALIGN_TOP);
        }

        public void createLoginPage(Widget widget) {
            layoutPage = new BasePage(BasePresenter.this);
            addCenter(widget);
        }

        public void createCreateTaskPage(Widget menu, Widget card) {
            clearCenter();
            addCenter(card);
            addWest(menu);
        }

        public void createSavedTasksPage(Widget menu, Widget card) {

        }

        public void createAdministrationPage(Widget menu, Widget card) {

        }

        @Override
        public void addNorth(IsWidget isWidget) {
            layoutPage.addNorth(isWidget);
        }

        @Override
        public void addNorth(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment) {
            layoutPage.addNorth(isWidget, horizontalAlignment);
        }

        @Override
        public void addNorth(IsWidget isWidget, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
            layoutPage.addNorth(isWidget, verticalAlignment);
        }

        @Override
        public void addNorth(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
            layoutPage.addNorth(isWidget, horizontalAlignment, verticalAlignment);
        }

        @Override
        public void addSouth(IsWidget isWidget) {
            layoutPage.addSouth(isWidget);
        }

        @Override
        public void addSouth(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment) {
            layoutPage.addSouth(isWidget, horizontalAlignment);
        }

        @Override
        public void addSouth(IsWidget isWidget, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
            layoutPage.addSouth(isWidget, verticalAlignment);
        }

        @Override
        public void addSouth(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
            layoutPage.addSouth(isWidget, horizontalAlignment, verticalAlignment);
        }

        @Override
        public void addWest(IsWidget isWidget) {
            layoutPage.addWest(isWidget);
        }

        @Override
        public void addWest(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment) {
            layoutPage.addWest(isWidget, horizontalAlignment);
        }

        @Override
        public void addWest(IsWidget isWidget, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
            layoutPage.addWest(isWidget, verticalAlignment);
        }

        @Override
        public void addWest(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
            layoutPage.addWest(isWidget, horizontalAlignment, verticalAlignment);
        }

        @Override
        public void addEast(IsWidget isWidget) {
            layoutPage.addEast(isWidget);
        }

        @Override
        public void addEast(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment) {
            layoutPage.addEast(isWidget, horizontalAlignment);
        }

        @Override
        public void addEast(IsWidget isWidget, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
            layoutPage.addEast(isWidget, verticalAlignment);
        }

        @Override
        public void addEast(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
            layoutPage.addEast(isWidget, horizontalAlignment, verticalAlignment);
        }

        @Override
        public void addCenter(IsWidget isWidget) {
            layoutPage.addCenter(isWidget);
        }

        @Override
        public void addCenter(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment) {
            layoutPage.addCenter(isWidget, horizontalAlignment);
        }

        @Override
        public void addCenter(IsWidget isWidget, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
            layoutPage.addCenter(isWidget, verticalAlignment);
        }

        @Override
        public void addCenter(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
            layoutPage.addCenter(isWidget, horizontalAlignment, verticalAlignment);
        }

        @Override
        public void clearNorth(IsWidget isWidget) {
            layoutPage.clearNorth(isWidget);
        }

        @Override
        public void clearSouth(IsWidget isWidget) {
            layoutPage.clearSouth(isWidget);
        }

        @Override
        public void clearWest(IsWidget isWidget) {
            layoutPage.clearWest(isWidget);
        }

        @Override
        public void clearEast(IsWidget isWidget) {
            layoutPage.clearEast(isWidget);
        }

        @Override
        public void clearCenter(IsWidget isWidget) {
            layoutPage.clearCenter(isWidget);
        }

        @Override
        public void clearNorth() {
            layoutPage.clearNorth();
        }

        @Override
        public void clearSouth() {
            layoutPage.clearSouth();
        }

        @Override
        public void clearWest() {
            layoutPage.clearWest();
        }

        @Override
        public void clearEast() {
            layoutPage.clearEast();
        }

        @Override
        public void clearCenter() {
            layoutPage.clearCenter();
        }
    }
}