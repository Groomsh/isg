package ru.javarush.isg.ui.shared.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import ru.javarush.isg.ui.shared.security.Credentials;

public class CredentialsValidatedEvent extends GwtEvent<CredentialsValidatedEvent.CredentialsValidatedEventHandler> {

    public interface CredentialsValidatedEventHandler extends EventHandler {
        public void onCredentialsValidated(CredentialsValidatedEvent event);
    }

    private final Credentials credentials;
    public static final Type<CredentialsValidatedEventHandler> TYPE = new Type<>();

    public Credentials getCredentials() {
        return credentials;
    }

    public CredentialsValidatedEvent(Credentials credentials) {
        this.credentials = credentials;
    }

    @Override
    public Type<CredentialsValidatedEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(CredentialsValidatedEventHandler handler) {
        handler.onCredentialsValidated(this);
    }


}
