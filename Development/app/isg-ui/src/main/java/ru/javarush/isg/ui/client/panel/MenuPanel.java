package ru.javarush.isg.ui.client.panel;

import com.github.gwtbootstrap.client.ui.NavLink;
import com.github.gwtbootstrap.client.ui.Navbar;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Widget;

@SuppressWarnings("unused")
public class MenuPanel implements IMenuPanel {

    protected interface MenuPanelUiBinder extends UiBinder<Navbar, MenuPanel> {}
    private static MenuPanelUiBinder ourUiBinder = GWT.create(MenuPanelUiBinder.class);

    @UiField
    protected Navbar root;
    @UiField
    protected NavLink createTask;
    @UiField
    protected NavLink savedTasks;
    @UiField
    protected NavLink administration;


    public MenuPanel() {
        ourUiBinder.createAndBindUi(this);
    }

    @Override
    public Widget asWidget() {
        return root;
    }

    /**
     * Get the source text from menu forms ie:
     * 1. Create Task
     * 2. Saved Tasks
     * 3. Administration
     * @param event just click event
     */
    @UiHandler(value = {"createTask", "savedTasks", "administration"})
    public void onCreateTaskClicked(ClickEvent event) {
        //TODO implement me
        System.out.println(event.getRelativeElement().getInnerText());
    }
}