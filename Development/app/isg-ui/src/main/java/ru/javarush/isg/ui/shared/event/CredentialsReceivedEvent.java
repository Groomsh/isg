package ru.javarush.isg.ui.shared.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import ru.javarush.isg.ui.shared.security.Credentials;

public class CredentialsReceivedEvent extends GwtEvent<CredentialsReceivedEvent.CredentialsReceivedEventHandler> {

    public interface CredentialsReceivedEventHandler extends EventHandler {
        public void onCredentialsReceived(CredentialsReceivedEvent event);
    }

    private final Credentials credentials;
    public static final Type<CredentialsReceivedEventHandler> TYPE = new Type<>();

    public Credentials getCredentials() {
        return credentials;
    }

    public CredentialsReceivedEvent(Credentials credentials) {
        this.credentials = credentials;
    }

    @Override
    public Type<CredentialsReceivedEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(CredentialsReceivedEventHandler handler) {
        handler.onCredentialsReceived(this);
    }
}
