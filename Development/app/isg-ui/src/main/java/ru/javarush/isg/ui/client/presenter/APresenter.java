package ru.javarush.isg.ui.client.presenter;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.ui.Widget;
import ru.javarush.isg.ui.shared.utils.ISGEventBus;

public abstract class APresenter implements IPresenter {

    private EventBus eventBus = ISGEventBus.getInstance();

    protected <T extends GwtEvent> void fireEvent(T event) {
        eventBus.fireEvent(event);
    }

    @SuppressWarnings("unchecked")
    protected <H> void addHandlers(H instance) {
        for (GwtEvent.Type<H> type : getTypes()) {
            eventBus.addHandler(type, instance);
        }
    }

    protected APresenter() {
        addHandlers(this);
    }

    /**
     * Return the event types for automatic handler registration;
     * You must implement {@link com.google.gwt.event.shared.EventHandler}
     * subtype, or ClassCastException will be thrown;
     * @return Event Types
     */
    protected abstract GwtEvent.Type[] getTypes();

    @Override
    public void unbind() {
        eventBus = null;
    }

    /**
     * Default implementation.
     * Usage: communication with view.
     * If you want a custom behavior implement this methods;
     */

    @Override
    public void update() {
        // do nothing
    }

    @Override
    public Widget getView() {
        // do nothing
        return null;
    }

    @Override
    public void onInput() {
        // do nothing
    }

    @Override
    public void onInput(String... params) {
        // do nothing
    }

    @Override
    public void onClick() {
        // do nothing
    }

    @Override
    public void onClick(String source) {
        // do nothing
    }
}
