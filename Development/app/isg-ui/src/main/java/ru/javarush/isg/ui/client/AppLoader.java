package ru.javarush.isg.ui.client;

import com.google.gwt.core.client.EntryPoint;
import ru.javarush.isg.ui.client.presenter.BasePresenter;
import ru.javarush.isg.ui.client.presenter.IPresenter;

public class AppLoader implements EntryPoint {

    @Override
    public void onModuleLoad() {
        IPresenter presenter = new BasePresenter();
        presenter.update();
    }

}
