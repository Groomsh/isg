package ru.javarush.isg.ui.shared.utils;

import com.google.gwt.event.shared.SimpleEventBus;

public class ISGEventBus extends SimpleEventBus {

    private static ISGEventBus eventBus = new ISGEventBus();

    public static ISGEventBus getInstance() {
        return eventBus;
    }

    private ISGEventBus() {}
}
