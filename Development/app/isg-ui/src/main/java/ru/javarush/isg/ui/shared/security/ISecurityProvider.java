package ru.javarush.isg.ui.shared.security;

public interface ISecurityProvider {

    public boolean validateCredentials(Credentials credentials);

}
