package ru.javarush.isg.ui.client.page;

import com.github.gwtbootstrap.client.ui.Hero;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;

/**
 * Компонент для отрисовки layout;
 */
@SuppressWarnings("unused")
public abstract class ABasePage extends Composite implements IBasePage {

    protected interface ABasePageUiBinder extends UiBinder<DockLayoutPanel, ABasePage> {}
    private static ABasePageUiBinder ourUiBinder = GWT.create(ABasePageUiBinder.class);

    @UiField
    protected HTMLPanel northPanel;
    @UiField
    protected Hero hero;
    @UiField
    protected VerticalPanel northContent;
    @UiField
    protected HorizontalPanel southContent;
    @UiField
    protected VerticalPanel westContent;
    @UiField
    protected VerticalPanel eastContent;
    @UiField
    protected HorizontalPanel centerContent;

    /**
     * Инициализации Root панели (DockLayoutPanel)
     */
    @UiConstructor
    public ABasePage() {
        //init
        DockLayoutPanel dockPanel = ourUiBinder.createAndBindUi(this);
        RootLayoutPanel rootPanel = RootLayoutPanel.get();
        initWidget(dockPanel);
        setVisible(true);
        rootPanel.add(this);
    }

    /**
     * Метод для получения header
     * @return panel of header zone
     */
    private VerticalPanel getNorthContent() {
        return northContent;
    }
    /**
     * Метод для получения footer
     * @return panel of footer zone
     */
    private HorizontalPanel getSouthContent() {
        return southContent;
    }
    /**
     * Метод для получения west zone
     * @return panel of west zone
     */
    private VerticalPanel getWestContent() {
        return westContent;
    }
    /**
     * Метод для получения east zone
     * @return panel of east zone
     */
    private VerticalPanel getEastContent() {
        return eastContent;
    }
    /**
     * Метод для получения center zone
     * @return panel of center zone
     */
    private HorizontalPanel getCenterContent() {
        return centerContent;
    }

    @Override
    public void addNorth(IsWidget isWidget) {
        addWidget(getNorthContent(), isWidget);
    }
    @Override
    public void addNorth(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment) {
        addWidget(getNorthContent(), isWidget, horizontalAlignment);
    }
    @Override
    public void addNorth(IsWidget isWidget, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
        addWidget(getNorthContent(), isWidget, verticalAlignment);
    }
    @Override
    public void addNorth(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment,
                            HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
        addWidget(getNorthContent(), isWidget, horizontalAlignment, verticalAlignment);
    }
    @Override
    public void addSouth(IsWidget isWidget) {
        addWidget(getSouthContent(), isWidget);
    }
    @Override
    public void addSouth(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment) {
        addWidget(getSouthContent(), isWidget, horizontalAlignment);
    }
    @Override
    public void addSouth(IsWidget isWidget, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
        addWidget(getSouthContent(), isWidget, verticalAlignment);
    }
    @Override
    public void addSouth(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment,
                            HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
        addWidget(getSouthContent(), isWidget, horizontalAlignment, verticalAlignment);
    }
    @Override
    public void addWest(IsWidget isWidget) {
        addWidget(getWestContent(), isWidget);
    }
    @Override
    public void addWest(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment) {
        addWidget(getWestContent(), isWidget, horizontalAlignment);
    }
    @Override
    public void addWest(IsWidget isWidget, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
        addWidget(getWestContent(), isWidget, verticalAlignment);
    }
    @Override
    public void addWest(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment,
                           HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
        addWidget(getWestContent(), isWidget, horizontalAlignment, verticalAlignment);
    }
    @Override
    public void addEast(IsWidget isWidget) {
        addWidget(getEastContent(), isWidget);
    }
    @Override
    public void addEast(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment) {
        addWidget(getEastContent(), isWidget, horizontalAlignment);
    }
    @Override
    public void addEast(IsWidget isWidget, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
        addWidget(getEastContent(), isWidget, verticalAlignment);
    }
    @Override
    public void addEast(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment,
                           HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
        addWidget(getEastContent(), isWidget, horizontalAlignment, verticalAlignment);
    }
    @Override
    public void addCenter(IsWidget isWidget) {
        addWidget(getCenterContent(), isWidget);
    }
    @Override
    public void addCenter(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment) {
        addWidget(getCenterContent(), isWidget, horizontalAlignment);
    }
    @Override
    public void addCenter(IsWidget isWidget, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
        addWidget(getCenterContent(), isWidget, verticalAlignment);
    }
    @Override
    public void addCenter(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment,
                             HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
        addWidget(getCenterContent(), isWidget, horizontalAlignment, verticalAlignment);
    }


    /**
     * Добавляет виджет в указанную зону.
     * @see #addWidget(CellPanel, IsWidget, HasHorizontalAlignment.HorizontalAlignmentConstant)
     * @see #addWidget(CellPanel, IsWidget, HasVerticalAlignment.VerticalAlignmentConstant)
     * @see #addWidget(CellPanel, IsWidget, HasHorizontalAlignment.HorizontalAlignmentConstant, HasVerticalAlignment.VerticalAlignmentConstant)
     * @param parent родительская зона
     * @param isWidget виджет для добавления
     */
    private void addWidget(CellPanel parent, IsWidget isWidget) {
        addWidget(parent, isWidget, HasHorizontalAlignment.ALIGN_CENTER, HasVerticalAlignment.ALIGN_MIDDLE);
    }

    /**
     * Добавляет виджет в указанную зону.
     * Можно указать горизонтальное расположение.
     * @see #addWidget(CellPanel, IsWidget)
     * @see #addWidget(CellPanel, IsWidget, HasVerticalAlignment.VerticalAlignmentConstant)
     * @see #addWidget(CellPanel, IsWidget, HasHorizontalAlignment.HorizontalAlignmentConstant, HasVerticalAlignment.VerticalAlignmentConstant)
     * @param parent родительская зона
     * @param isWidget виджет для добавления
     * @param horizontalAlignment константа горизонтального расположения
     */
    private void addWidget(CellPanel parent, IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment) {
        addWidget(parent, isWidget, horizontalAlignment, HasVerticalAlignment.ALIGN_MIDDLE);
    }

    /**
     * Добавляет виджет в указанную зону.
     * Можно указать горизонтальное и вертикальное расположение.
     * @see #addWidget(CellPanel, IsWidget)
     * @see #addWidget(CellPanel, IsWidget, HasHorizontalAlignment.HorizontalAlignmentConstant)
     * @see #addWidget(CellPanel, IsWidget, HasVerticalAlignment.VerticalAlignmentConstant)
     * @param parent  родительская зона
     * @param isWidget виджет для добавления
     * @param horizontalAlignment константа горизонтального расположения
     * @param verticalAlignment константа вертикального расположения
     */
    private void addWidget(CellPanel parent, IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment,
                           HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
        parent.add(isWidget);
        parent.setCellHorizontalAlignment(isWidget, horizontalAlignment);
        parent.setCellVerticalAlignment(isWidget, verticalAlignment);
    }

    /**
     * Добавляет виджет в указанную зону.
     * Можно указать вертикальное расположение.
     * @see #addWidget(CellPanel, IsWidget)
     * @see #addWidget(CellPanel, IsWidget, HasHorizontalAlignment.HorizontalAlignmentConstant)
     * @see #addWidget(CellPanel, IsWidget, HasHorizontalAlignment.HorizontalAlignmentConstant, HasVerticalAlignment.VerticalAlignmentConstant)
     * @param parent родительская зона
     * @param isWidget виджет для добавления
     * @param verticalAlignment константа вертикального расположения
     */
    private void addWidget(CellPanel parent, IsWidget isWidget, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment) {
        addWidget(parent, isWidget, HasHorizontalAlignment.ALIGN_CENTER, verticalAlignment);
    }

    /**
     * Удаляет переданный виджет из указанного контейнера.
     * @param isWidget виджет для удаления
     * @param parent родительский контейнер
     */
    private void clearWidget(CellPanel parent, IsWidget isWidget) {
        parent.remove(isWidget);
    }
    @Override
    public void clearNorth(IsWidget isWidget) {
        clearWidget(getNorthContent(), isWidget);
    }
    @Override
    public void clearSouth(IsWidget isWidget) {
        clearWidget(getSouthContent(), isWidget);
    }
    @Override
    public void clearWest(IsWidget isWidget) {
        clearWidget(getWestContent(), isWidget);
    }
    @Override
    public void clearEast(IsWidget isWidget) {
        clearWidget(getEastContent(), isWidget);
    }
    @Override
    public void clearCenter(IsWidget isWidget) {
        clearWidget(getCenterContent(), isWidget);
    }

    /**
     * Clear target panel from widgets.
     * @param parent родительский контейнер
     */
    private void clearWidget(CellPanel parent) {
        parent.clear();
    }
    @Override
    public void clearNorth() {
        clearWidget(getNorthContent());
    }
    @Override
    public void clearSouth() {
        clearWidget(getSouthContent());
    }
    @Override
    public void clearWest() {
        clearWidget(getWestContent());
    }
    @Override
    public void clearEast() {
        clearWidget(getEastContent());
    }
    @Override
    public void clearCenter() {
        clearWidget(getCenterContent());
    }

}