package ru.javarush.isg.ui.shared.security;

public class StubSecurityProvider implements ISecurityProvider {

    @Override
    public boolean validateCredentials(Credentials credentials) {

        if (credentials.getUsername() == null || credentials.getPassword() == null) {
            return false;
        }
        //one stub role
        return credentials.getUsername().equalsIgnoreCase("Admin") && credentials.getPassword().equals("1234");
    }
}
