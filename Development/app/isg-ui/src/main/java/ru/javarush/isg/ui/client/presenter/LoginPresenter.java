package ru.javarush.isg.ui.client.presenter;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.ui.Widget;
import ru.javarush.isg.ui.client.card.ILoginCard;
import ru.javarush.isg.ui.client.card.LoginCard;
import ru.javarush.isg.ui.shared.event.CredentialsReceivedEvent;
import ru.javarush.isg.ui.shared.event.CredentialsValidatedEvent;
import ru.javarush.isg.ui.shared.security.Credentials;
import ru.javarush.isg.ui.shared.security.ISecurityProvider;
import ru.javarush.isg.ui.shared.security.StubSecurityProvider;

public class LoginPresenter extends APresenter implements CredentialsReceivedEvent.CredentialsReceivedEventHandler {

    private final ILoginCard loginCard = LoginCard.getInstance(this);
    private final ISecurityProvider securityProvider = new StubSecurityProvider();

    public LoginPresenter() {

    }

    @Override
    protected GwtEvent.Type[] getTypes() {
        return new GwtEvent.Type[]{CredentialsReceivedEvent.TYPE};
    }

    @Override
    public void onCredentialsReceived(CredentialsReceivedEvent event) {
        if (securityProvider.validateCredentials(event.getCredentials())) {
            fireEvent(new CredentialsValidatedEvent(event.getCredentials()));
        } else {
            loginCard.alert();
        }
    }

    @Override
    public void onInput(String... params) {
        //2 for username and password
        if (params == null || params.length != 2) {
            return;
        }
        fireEvent(new CredentialsReceivedEvent(new Credentials(params[0], params[1])));
    }

    @Override
    public Widget getView() {
        return loginCard.asWidget();
    }
}
