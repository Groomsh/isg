package ru.javarush.isg.ui.client.presenter;
import com.google.gwt.user.client.ui.Widget;

@SuppressWarnings("unused")
public interface IPresenter {

    /**
     * Use this method for retrieving data from server side;
     */
    public void update();

    public Widget getView();

    public void onInput();

    public void onInput(String... params);

    public void onClick();

    /**
     * @param source text containing in clicked widget;
     */
    public void onClick(String source);

    /**
     * Call unbind before create next presenter of the same type
     * for cleaning the links for GC
     */
    public void unbind();

}
