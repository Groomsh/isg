package ru.javarush.isg.ui.client.page;

import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.IsWidget;

public interface IBasePage {

    public void addNorth(IsWidget isWidget);
    public void addNorth(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment);
    public void addNorth(IsWidget isWidget, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment);
    public void addNorth(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment);
    public void addSouth(IsWidget isWidget);
    public void addSouth(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment);
    public void addSouth(IsWidget isWidget, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment);
    public void addSouth(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment);
    public void addWest(IsWidget isWidget);
    public void addWest(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment);
    public void addWest(IsWidget isWidget, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment);
    public void addWest(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment);
    public void addEast(IsWidget isWidget);
    public void addEast(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment);
    public void addEast(IsWidget isWidget, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment);
    public void addEast(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment);
    public void addCenter(IsWidget isWidget);
    public void addCenter(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment);
    public void addCenter(IsWidget isWidget, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment);
    public void addCenter(IsWidget isWidget, HasHorizontalAlignment.HorizontalAlignmentConstant horizontalAlignment, HasVerticalAlignment.VerticalAlignmentConstant verticalAlignment);
    public void clearNorth(IsWidget isWidget);
    public void clearSouth(IsWidget isWidget);
    public void clearWest(IsWidget isWidget);
    public void clearEast(IsWidget isWidget);
    public void clearCenter(IsWidget isWidget);
    public void clearNorth();
    public void clearSouth();
    public void clearWest();
    public void clearEast();
    public void clearCenter();

}
