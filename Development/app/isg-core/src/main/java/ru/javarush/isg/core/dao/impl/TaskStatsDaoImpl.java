package ru.javarush.isg.core.dao.impl;

import ru.javarush.isg.core.dao.TaskStatsDao;
import ru.javarush.isg.core.model.TaskStats;
import ru.javarush.isg.core.model.TaskStats_;

import javax.ejb.Stateless;

@SuppressWarnings("unused")
@Stateless
public class TaskStatsDaoImpl extends ADaoImpl<TaskStats, Long> implements TaskStatsDao {

    public TaskStatsDaoImpl() {
        super(TaskStats_.id);
    }

}
