package ru.javarush.isg.core.dao.impl;

import ru.javarush.isg.core.dao.UserDao;
import ru.javarush.isg.core.model.User;
import ru.javarush.isg.core.model.User_;

import javax.ejb.Stateless;

@SuppressWarnings("unused")
@Stateless
public class UserDaoImpl extends ADaoImpl<User, Long> implements UserDao {

    public UserDaoImpl() {
        super(User_.id);
    }
}
