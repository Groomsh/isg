package ru.javarush.isg.core.dao;

import ru.javarush.isg.core.model.Statistic;

import javax.ejb.Local;

@Local
public interface StatisticDao extends Dao<Statistic, Long> {

}
