package ru.javarush.isg.core.dao.impl;

import ru.javarush.isg.core.dao.TaskDao;
import ru.javarush.isg.core.model.Task;
import ru.javarush.isg.core.model.Task_;

import javax.ejb.Stateless;


@SuppressWarnings("unused")
@Stateless
public class TaskDaoImpl extends ADaoImpl<Task, Long> implements TaskDao {

    public TaskDaoImpl() {
        super(Task_.id);
    }

}
