package ru.javarush.isg.core.dao.impl;

import ru.javarush.isg.core.dao.StatisticDao;
import ru.javarush.isg.core.model.Statistic;
import ru.javarush.isg.core.model.Statistic_;

import javax.ejb.Stateless;

@SuppressWarnings("unused")
@Stateless
public class StatisticDaoImpl extends ADaoImpl<Statistic, Long> implements StatisticDao {

    public StatisticDaoImpl() {
        super(Statistic_.id);
    }
}
