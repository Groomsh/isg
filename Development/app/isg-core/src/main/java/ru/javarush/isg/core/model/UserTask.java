package ru.javarush.isg.core.model;

import javax.persistence.*;
import java.util.Date;

@SuppressWarnings("all")
@Entity
@Table(name = "usertask")
public class UserTask {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "userId")
    private Long userId;

    @Column(name = "taskId")
    private Long taskId;

    @Column(name = "status")
    private Long status;

    @Column(name = "attempt")
    private Long attempt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createdTime")
    private Date createdTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updatedTime")
    private Date updatedTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getAttempt() {
        return attempt;
    }

    public void setAttempt(Long attempt) {
        this.attempt = attempt;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }
}
