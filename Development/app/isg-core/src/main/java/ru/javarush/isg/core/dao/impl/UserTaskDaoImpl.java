package ru.javarush.isg.core.dao.impl;

import ru.javarush.isg.core.dao.UserTaskDao;
import ru.javarush.isg.core.model.UserTask;
import ru.javarush.isg.core.model.UserTask_;

import javax.ejb.Stateless;

@SuppressWarnings("unused")
@Stateless
public class UserTaskDaoImpl extends ADaoImpl<UserTask, Long> implements UserTaskDao {

    public UserTaskDaoImpl() {
        super(UserTask_.id);
    }

}
