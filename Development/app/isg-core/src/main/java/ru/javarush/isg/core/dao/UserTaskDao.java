package ru.javarush.isg.core.dao;

import ru.javarush.isg.core.model.UserTask;

import javax.ejb.Local;

@Local
public interface UserTaskDao extends Dao<UserTask, Long> {

}
