package ru.javarush.isg.core.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@SuppressWarnings("unused")
@StaticMetamodel(TaskStats.class)
public class TaskStats_ {

    public static volatile SingularAttribute<TaskStats, Long> id;
    public static volatile SingularAttribute<TaskStats, String> keyTask;
    public static volatile SingularAttribute<TaskStats, Long> level;
    public static volatile SingularAttribute<TaskStats, Long> usrUnsolved;
    public static volatile SingularAttribute<TaskStats, Long> attemptUnsolved;
    public static volatile SingularAttribute<TaskStats, Long> usrSolved;
    public static volatile SingularAttribute<TaskStats, Long> attemptSolved;
    public static volatile SingularAttribute<TaskStats, Long> maxAttempts;
    public static volatile SingularAttribute<TaskStats, String> title;

}
