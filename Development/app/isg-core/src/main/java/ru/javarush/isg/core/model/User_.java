package ru.javarush.isg.core.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

@SuppressWarnings("unused")
@StaticMetamodel(User.class)
public class User_ {

    public static volatile SingularAttribute<User, Long> id;
    public static volatile SingularAttribute<User, String> name;
    public static volatile SingularAttribute<User, String> pictureUrl;
    public static volatile SingularAttribute<User, String> city;
    public static volatile SingularAttribute<User, Long> level;
    public static volatile SingularAttribute<User, Long> lesson;
    public static volatile SingularAttribute<User, Long> gold;
    public static volatile SingularAttribute<User, Long> silver;
    public static volatile SingularAttribute<User, Long> experience;
    public static volatile SingularAttribute<User, Date> createdTime;
    public static volatile SingularAttribute<User, Date> updatedTime;

}
