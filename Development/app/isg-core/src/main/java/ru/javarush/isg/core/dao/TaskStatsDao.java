package ru.javarush.isg.core.dao;

import ru.javarush.isg.core.model.TaskStats;

import javax.ejb.Local;

@Local
public interface TaskStatsDao extends Dao<TaskStats, Long> {

}
