package ru.javarush.isg.core.model;

import javax.persistence.*;
import java.util.Date;

@SuppressWarnings("all")
@Entity
@Table(name = "tasks")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "key2")
    private String key;

    @Column(name = "level")
    private Long level;

    @Column(name = "lesson")
    private Long lesson;

    @Column(name = "number")
    private Long number;

    @Column(name = "title")
    private String title;

    @Column(name = "gold")
    private Long gold;

    @Column(name = "silver")
    private Long silver;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createdTime")
    private Date createdTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updatedTime")
    private Date updatedTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public Long getLesson() {
        return lesson;
    }

    public void setLesson(Long lesson) {
        this.lesson = lesson;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getGold() {
        return gold;
    }

    public void setGold(Long gold) {
        this.gold = gold;
    }

    public Long getSilver() {
        return silver;
    }

    public void setSilver(Long silver) {
        this.silver = silver;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }
}
