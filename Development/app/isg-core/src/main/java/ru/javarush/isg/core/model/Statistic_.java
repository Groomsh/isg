package ru.javarush.isg.core.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

@SuppressWarnings("unused")
@StaticMetamodel(Statistic.class)
public class Statistic_ {

    public static volatile SingularAttribute<Statistic, Long> id;
    public static volatile SingularAttribute<Statistic, Long> userId;
    public static volatile SingularAttribute<Statistic, Long> actionType;
    public static volatile SingularAttribute<Statistic, String> message;
    public static volatile SingularAttribute<Statistic, Date> createdTime;
    public static volatile SingularAttribute<Statistic, Date> updatedTime;

}
