package ru.javarush.isg.core.dao;

import ru.javarush.isg.core.model.User;

import javax.ejb.Local;

@Local
public interface UserDao extends Dao<User, Long> {

}
