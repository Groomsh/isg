package ru.javarush.isg.core.dao.impl;

import ru.javarush.isg.core.dao.Dao;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

@SuppressWarnings("all")
public abstract class ADaoImpl<T, PK extends Serializable> implements Dao<T, PK> {

    @Inject
    protected EntityManager entityManager;

    protected Class<T> entityClass;

    protected final SingularAttribute<? super T, PK> idAttribute;

    public ADaoImpl(SingularAttribute<? super T, PK> idAttribute) {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
        this.idAttribute = idAttribute;
    }

    @Override
    public void flush() {
        entityManager.flush();
    }

    @Override
    public T create(T t) {
        entityManager.persist(t);
        return t;
    }

    @Override
    public T getById(PK id) {
        return entityManager.find(entityClass, id);
    }

    @Override
    public T update(T t) {
        return entityManager.merge(t);
    }

    @Override
    public void delete(T t) {
        entityManager.remove(entityManager.merge(t));
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public T createAndCommit(T t) {
        return create(t);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public T updateAndCommit(T t) {
        return update(t);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void deleteAndCommit(T t) {
        delete(t);
    }

    @Override
    public List<T> listAll() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(entityClass);
        Root<T> root = query.from(entityClass);
        query.select(root);
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public <A> List<T> listBy(SingularAttribute<? super T, A> attribute, A value) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(entityClass);
        Root<T> root = query.from(entityClass);

        query.select(root).where(builder.equal(root.get(attribute), value));

        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public <A> T getBy(SingularAttribute<? super T, A> attribute, A value) {
        List<T> list = listBy(attribute, value);
        return list.isEmpty() ? null : list.get(0);
    }

    @Override
    public long countAll() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<T> root = query.from(entityClass);
        query.select(builder.count(root));
        return entityManager.createQuery(query).getSingleResult();
    }

    @Override
    public <A> long countBy(SingularAttribute<? super T, A> attribute, A value) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<T> root = query.from(entityClass);
        query.select(builder.count(root)).where(builder.equal(root.get(attribute), value));
        return entityManager.createQuery(query).getSingleResult();
    }

}
