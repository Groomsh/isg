package ru.javarush.isg.core.model;

import javax.persistence.*;

@SuppressWarnings("all")
@Entity
@Table(name = "task_stats")
public class TaskStats {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "keyTask")
    private String keyTask;

    @Column(name = "level")
    private Long level;

    @Column(name = "usrUnsolved")
    private Long usrUnsolved;

    @Column(name = "attemptUnsolved")
    private Long attemptUnsolved;

    @Column(name = "usrSolved")
    private Long usrSolved;

    @Column(name = "attemptSolved")
    private Long attemptSolved;

    @Column(name = "maxAttempts")
    private Long maxAttempts;

    @Column(name = "title")
    private String title;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKeyTask() {
        return keyTask;
    }

    public void setKeyTask(String keyTask) {
        this.keyTask = keyTask;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public Long getUsrUnsolved() {
        return usrUnsolved;
    }

    public void setUsrUnsolved(Long usrUnsolved) {
        this.usrUnsolved = usrUnsolved;
    }

    public Long getAttemptUnsolved() {
        return attemptUnsolved;
    }

    public void setAttemptUnsolved(Long attemptUnsolved) {
        this.attemptUnsolved = attemptUnsolved;
    }

    public Long getUsrSolved() {
        return usrSolved;
    }

    public void setUsrSolved(Long usrSolved) {
        this.usrSolved = usrSolved;
    }

    public Long getAttemptSolved() {
        return attemptSolved;
    }

    public void setAttemptSolved(Long attemptSolved) {
        this.attemptSolved = attemptSolved;
    }

    public Long getMaxAttempts() {
        return maxAttempts;
    }

    public void setMaxAttempts(Long maxAttempts) {
        this.maxAttempts = maxAttempts;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
