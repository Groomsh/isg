package ru.javarush.isg.core.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

@SuppressWarnings("unused")
@StaticMetamodel(UserTask.class)
public class UserTask_ {

    public static volatile SingularAttribute<UserTask, Long> id;
    public static volatile SingularAttribute<UserTask, Long> userId;
    public static volatile SingularAttribute<UserTask, Long> taskId;
    public static volatile SingularAttribute<UserTask, Long> status;
    public static volatile SingularAttribute<UserTask, Long> attempt;
    public static volatile SingularAttribute<UserTask, Date> createdTime;
    public static volatile SingularAttribute<UserTask, Date> updatedTime;

}
