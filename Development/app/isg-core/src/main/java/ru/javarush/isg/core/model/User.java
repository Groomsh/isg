package ru.javarush.isg.core.model;

import javax.persistence.*;
import java.util.Date;

@SuppressWarnings("all")
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "displayName")
    private String name;

    @Column(name = "pictureUrl")
    private String pictureUrl;

    @Column(name = "city")
    private String city;

    @Column(name = "level")
    private Long level;

    @Column(name = "lesson")
    private Long lesson;

    @Column(name = "gold")
    private Long gold;

    @Column(name = "silver")
    private Long silver;

    @Column(name = "experience")
    private Long experience;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createdTime")
    private Date createdTime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updatedTime")
    private Date updatedTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public Long getLesson() {
        return lesson;
    }

    public void setLesson(Long lesson) {
        this.lesson = lesson;
    }

    public Long getGold() {
        return gold;
    }

    public void setGold(Long gold) {
        this.gold = gold;
    }

    public Long getSilver() {
        return silver;
    }

    public void setSilver(Long silver) {
        this.silver = silver;
    }

    public Long getExperience() {
        return experience;
    }

    public void setExperience(Long experience) {
        this.experience = experience;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }
}
