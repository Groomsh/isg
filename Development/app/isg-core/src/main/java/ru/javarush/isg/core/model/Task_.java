package ru.javarush.isg.core.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

@SuppressWarnings("unused")
@StaticMetamodel(Task.class)
public class Task_ {

    public static volatile SingularAttribute<Task, Long> id;
    public static volatile SingularAttribute<Task, String> key;
    public static volatile SingularAttribute<Task, Long> level;
    public static volatile SingularAttribute<Task, Long> lesson;
    public static volatile SingularAttribute<Task, Long> number;
    public static volatile SingularAttribute<Task, String> title;
    public static volatile SingularAttribute<Task, Long> gold;
    public static volatile SingularAttribute<Task, Long> silver;
    public static volatile SingularAttribute<Task, Date> createdTime;
    public static volatile SingularAttribute<Task, Date> updatedTime;

}
