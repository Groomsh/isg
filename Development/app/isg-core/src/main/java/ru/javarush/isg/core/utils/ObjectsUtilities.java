package ru.javarush.isg.core.utils;

import com.google.common.base.Preconditions;

import java.util.Objects;

/**
 * Service class for simplify all the common stuff;
 * It's aggregates useful methods from Objects and Guava library's;
 * Use static import for clean code view;
 * Most of the methods are fail fast.
 */

@SuppressWarnings("unused")
public class ObjectsUtilities {

    public static <T> T requireNonNull(T object) {
       return Objects.requireNonNull(object);
    }

    public static <T> T requireNonNull(T object, String errorMessage) {
        return Objects.requireNonNull(object, errorMessage);
    }

    public static void checkArgument(boolean expression) {
        Preconditions.checkArgument(expression);
    }

    public static void checkArgument(boolean expression, String errorMessage) {
        Preconditions.checkArgument(expression, errorMessage);
    }

    public static boolean equal(Object first, Object second) {
        return Objects.equals(first, second);
    }

    public static boolean isNull(Object o) {
        return Objects.isNull(o);
    }

    public static boolean nonNull(Object o) {
        return Objects.nonNull(o);
    }

    public static int hash(Object... values) {
        return Objects.hash(values);
    }

    public static String toString(Object o) {
        return Objects.toString(o);
    }

    public static String toString(Object o, String nullDefault) {
        return Objects.toString(o, nullDefault);
    }

}
