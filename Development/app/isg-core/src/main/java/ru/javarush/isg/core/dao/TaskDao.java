package ru.javarush.isg.core.dao;

import ru.javarush.isg.core.model.Task;

import javax.ejb.Local;

@Local
public interface TaskDao extends Dao<Task, Long> {

}
