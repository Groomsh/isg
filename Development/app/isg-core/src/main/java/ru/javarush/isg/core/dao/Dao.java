package ru.javarush.isg.core.dao;

import java.io.Serializable;
import java.util.List;

import javax.ejb.*;
import javax.persistence.metamodel.*;

@SuppressWarnings("unused")
public interface Dao<T, PK extends Serializable> {

    public void flush();

    public T create(T t);

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public T createAndCommit(T t);

    public T getById(PK id);

    public T update(T t);

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public T updateAndCommit(T t);

    public void delete(T t);

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void deleteAndCommit(T t);

    public long countAll();

    public List<T> listAll();

    public <A> List<T> listBy(SingularAttribute<? super T, A> attribute, A value);

    public <A> T getBy(SingularAttribute<? super T, A> attribute, A value);

    public <A> long countBy(SingularAttribute<? super T, A> attribute, A value);
}